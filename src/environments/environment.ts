// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyByEIxTo1BoUJA8CdqXyE3c6EuzyGevYKQ",
    authDomain: "gestiontaches-aab9d.firebaseapp.com",
    projectId: "gestiontaches-aab9d",
    storageBucket: "gestiontaches-aab9d.appspot.com",
    messagingSenderId: "478700539877",
    appId: "1:478700539877:web:2ced7a66f2abede03ee90c"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
