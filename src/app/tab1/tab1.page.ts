import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { LoadingController, Platform } from '@ionic/angular';
//import * as firebase from 'firebase';
import  firebase from 'firebase';
import { Tache } from '../models/tache';
import { TacheService } from '../services/tache.service';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  public loading: any;
  public isGoogleLogin = false;
  public user = null;
  showTable = false;
  etat = "A faire";
  taches : Tache[];
  titre:string = "Login"
  isNewTache = false;
  isUpdateTache = false;
  tache:Tache = new Tache();
  etats: any[] = [];
  tacheUpdate : Tache;
  showError = false;
  tachForm: FormGroup;
  nom:string=""
  constructor(
    private google: GooglePlus,
    public loadingController: LoadingController,
    private fireAuth: AngularFireAuth,
    private platform: Platform,
    public fb: TacheService,
  ) {}

  async ngOnInit() {
    this.tachForm = new FormGroup({
      nom: new FormControl('', Validators.required)
   });
    this.etats.push('A faire', 'Terminer')
    this.loading = await this.loadingController.create({
      message: 'Connecting ...'
    });
  }
  doLogin(){
    let params: any;
    if (this.platform.is('cordova')) {
      if (this.platform.is('android')) {
        params = {
          webClientId: '245600669845-57f689bh9j29ijjcsvnhd0ekehjojh94.apps.googleusercontent.com',
          offline: true
        };
      } else {
        params = {};
      }
      this.google.login(params)
      .then((response) => {
        const { idToken, accessToken } = response;
        this.onLoginSuccess(idToken, accessToken);
      }).catch((error) => {
        console.log(error);
        alert('error:' + JSON.stringify(error));
      });
    } else{
      console.log('else...');
      this.fireAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then(success => {
        console.log(success);
        this.isGoogleLogin = true;
        this.user =  success.user;
        this.titre = "Bonjour "+this.user.displayName;
        this.fb.idUser = this.user.uid;
        this.getListeTacheByIdUser(this.fb.idUser);
      }).catch(err => {
        console.log(err.message);
      });
    }
  }
  onLoginSuccess(accessToken, accessSecret) {
    const credential = accessSecret ? firebase.auth.GoogleAuthProvider
        .credential(accessToken, accessSecret) : firebase.auth.GoogleAuthProvider
            .credential(accessToken);
    this.fireAuth.signInWithCredential(credential)
      .then((success) => {
        this.isGoogleLogin = true;
        this.user =  success.user;
        this.loading.dismiss();
      });

  }
  onLoginError(err) {
    console.log(err);
  }
  logout() {
    this.fireAuth.signOut().then(() => {
      this.isGoogleLogin = false;
      this.titre = "Login";
    });
  }

  getListeTacheByIdUser(idUser:string) {
    this.fb.getById(idUser).subscribe(data => {
      this.taches = data.map(e => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data()
        } as Tache;
      })
      if (this.taches.length != 0) 
        this.showTable = true;
      else
        this.showTable = false;
    });
    
  }
  AddTache(tache:Tache) {
    if (this.nom === "") {
      this.showError=true;
    }else{
      tache.nom = this.nom;
      tache.etat = this.etat;
      tache.idUser = this.fb.idUser;
      this.save(tache);
      this.isNewTache = false;
      this.getListeTacheByIdUser(this.fb.idUser);
    }
      
  }

  save(tache:Tache) {
    const obj = Object.assign({},tache)
    this.fb.createTache(obj).then((res)=>{
      }).catch((error) =>{
        console.log("erroooooor****",error);
      });
  }

  IsAddTache(){
    this.isNewTache = true;
    this.isUpdateTache = false;
    this.tache = new Tache();
  }
  isUpdate(tache:Tache){
    this.isUpdateTache = true;
    this.isNewTache = false;
    this.tacheUpdate = tache;
  }
  update(tache : Tache){
    this.fb.updateTache(tache);
    this.isUpdateTache = false;
  }

  termineAll(){
    this.taches.forEach(tache => { 
      if (tache.etat == "A faire") {
        tache.etat="Terminer"
        this.fb.updateTache(tache);
      }
    });
  }

}
