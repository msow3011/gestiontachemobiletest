import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Tache } from '../models/tache';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class TacheService {

  idUser : string;
  displayeName : string ;
  email : string ;
  constructor(private firestore: AngularFirestore) { } 

  
  getById(idUser:string) : Observable<any> {
    console.log("id----------------",idUser);
     return this.firestore.collection("taches", ref=> ref.where("idUser", "==", idUser)).snapshotChanges();
  }

  getTaches() : Observable<any> {
    return this.firestore.collection('taches').snapshotChanges();
  }


  createTache(tache: any){
    return this.firestore.collection('taches').add(tache);
  }

  deleteTache(tacheId: string){
    this.firestore.doc('taches/' + tacheId).delete();
  }
  
  updateTache(tache: Tache){
    const obj = Object.assign({},tache)
    console.log("tache update*************",obj);
    delete obj.id;
    this.firestore.doc('taches/'+tache.id).update(obj);
  }
}
